package com.example.rentinator;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingActivity extends AppCompatActivity {

    private Button update;
    private EditText uName,uEmail,uPass,uCon;
    private CircleImageView uImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        getSupportActionBar().setTitle("Setting");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        update = (Button)findViewById(R.id.set_user_button);
        uName = (EditText) findViewById(R.id.set_user_name);
        uEmail = (EditText) findViewById(R.id.set_user_email);
        uPass = (EditText) findViewById(R.id.set_user_Password);
        uCon = (EditText) findViewById(R.id.set_user_contact);
        uImage= (CircleImageView) findViewById(R.id.profile_image);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
             default:
                 return super.onOptionsItemSelected(item);
        }
    }




}
