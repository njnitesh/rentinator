package com.example.rentinator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class frag3test extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frag3test);
        Bundle bundle= frag3test.this.getIntent().getExtras();

        String tempHolder = bundle.getString("val");
        Toast.makeText(getApplicationContext(), tempHolder, Toast.LENGTH_SHORT).show();
    }
}
