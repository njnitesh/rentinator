package com.example.rentinator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import static com.example.rentinator.R.layout.abc_cascading_menu_item_layout;
import static com.example.rentinator.R.layout.custom;


public class frag2 extends Fragment {
    View view;
    EditText itemName, quantity, details, price;
    Spinner categorySpinner;
    Button uploadImage, gallery;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag2_layout, container, false);


        itemName = (EditText) view.findViewById(R.id.etItemName);
        quantity = (EditText) view.findViewById(R.id.etQuantity);
        details = (EditText) view.findViewById(R.id.etDetails);
        price = (EditText) view.findViewById(R.id.etPrice);
        uploadImage = (Button) view.findViewById(R.id.btnUpload);
        gallery = (Button) view.findViewById(R.id.btnGallery);
        categorySpinner = (Spinner) view.findViewById(R.id.spinner);

        String[] mApps = {"Cars","Bikes","Mobiles","Furniture","Electronics and Appliances", "Stationery", "Fashion", "Sports" };

        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(Objects.requireNonNull(this.getActivity()), android.R.layout.simple_spinner_item,mApps);
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(myAdapter);


        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getApplicationContext = null;
                Intent i = new Intent(null,UploadPicActivity.class);
                startActivity(i);
            }
        });


        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getApplicationContext = null;
                Intent i = new Intent(null,GalleryActivity.class);
                startActivity(i);
            }
        });


        return view;
    }
}


