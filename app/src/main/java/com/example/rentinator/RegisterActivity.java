package com.example.rentinator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

import static android.provider.AlarmClock.EXTRA_MESSAGE;


public class RegisterActivity extends AppCompatActivity {
    private Firebase root;
    private Button submit;
    private EditText name, mail, contact, cPass, pwd, add;
    private static final String fileName = "userId.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        root = new Firebase("https://rentinator-5ff21.firebaseio.com/users");
        //Firebase.setAndroidContext(this);

        name = (EditText) findViewById(R.id.etName) ;
        mail = (EditText) findViewById(R.id.etEmail);
        add = (EditText) findViewById(R.id.etaddress);
        contact = (EditText) findViewById(R.id.etContact);
        cPass = (EditText) findViewById(R.id.etConfirmPassword);
        pwd = (EditText) findViewById(R.id.etPassword);
        submit = (Button) findViewById(R.id.button1);

        submit.setOnClickListener(new View.OnClickListener(){
            public void onClick(View V) {

                String nData = name.getText().toString();
                String mData = mail.getText().toString();
                String cData = contact.getText().toString();
                String pData = pwd.getText().toString();
                String cpData = cPass.getText().toString();
                String aData = add.getText().toString();

                if (CheckUserExistance()) {
                    Toast.makeText(RegisterActivity.this, "The user already exists", Toast.LENGTH_SHORT).show();
                } else {
                    if (cpData.equals(pData)) {
                        if (cData.length() == 10) {
                            double rand = Math.random();
                            String sRand = String.valueOf(rand).replace(".", "");

                            Firebase rChild = root.child(sRand);

                            Toast.makeText(getApplicationContext(), sRand, Toast.LENGTH_SHORT).show();

                            Firebase rChildEmail = rChild.child("Email");
                            rChildEmail.setValue(mData);

                            Firebase rChildName = rChild.child("Name");
                            rChildName.setValue(nData);

                            Firebase rChildContact = rChild.child("Contact");
                            rChildContact.setValue(cData);

                            Firebase rChildPwd = rChild.child("Password");
                            rChildPwd.setValue(pData);

                            Firebase rChildAdd = rChild.child("Address");
                            rChildAdd.setValue(aData);

                            try {
                                WriteData(sRand);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            Toast.makeText(getApplicationContext(), "Registration Successfully", Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                            finish();
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "The Phone number should be exactly 10 digits", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "The passwords do not match", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

    }

    private void WriteData(String id) throws IOException
    {
        FileOutputStream fos = null;
        try
        {
            fos = openFileOutput(fileName, MODE_PRIVATE); // Mode Private makes the file such that only our app can access it.
            fos.write(id.getBytes());
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }

        finally
        {
            if(fos != null)
            {
                fos.close();
            }
        }
    }

    private boolean CheckUserExistance(){
        File file = new File(getFilesDir()+"/usersId.txt");
        if(file.exists()){
            return(true);
        }else{
            return(false);
        }
    }

}
