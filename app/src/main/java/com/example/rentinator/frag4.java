package com.example.rentinator;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class frag4 extends Fragment {
    View view;

    TextView etnm, etmail, etcon;
    ImageView pImageView;
    Button btncamera, btngallery;
    DataSnapshot dataSnapshot;
    DatabaseReference rootRef, demoRef;
    public String usr, mail, contact;
    private static final int CAMERA_REQUEST_CODE = 1;

    private StorageReference mStorage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag4test, container,false);

//        etnm = (TextView) view.findViewById(R.id.textView);
//        etmail = (TextView) view.findViewById(R.id.textView1);
//        etcon = (TextView) view.findViewById(R.id.textView2);
//        pImageView = (ImageView) view.findViewById(R.id.profile_pic);
//        btncamera = (Button) view.findViewById(R.id.button);
//        btngallery = (Button) view.findViewById(R.id.button1);

//        //database reference pointing to root of database
//        rootRef = FirebaseDatabase.getInstance().getReference();
//
//        //database reference pointing to demo node
//        demoRef = rootRef.child("demo");
//
//        demoRef.child("username").addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                usr = dataSnapshot.getValue(String.class);
//                etnm.setText(usr);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//            }
//        });
//
//        demoRef.child("mail").addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                mail = dataSnapshot.getValue(String.class);
//                etmail.setText(mail);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//            }
//        });
//
//        demoRef.child("phno").addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                contact = dataSnapshot.getValue(String.class);
//                etcon.setText(contact);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//            }
//        });
//        btncamera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
//        btngallery.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(intent, CAMERA_REQUEST_CODE);
//            }
//        });
        return view;
    }

    @Override

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {

            Toast.makeText(getActivity(), "Uploading Image...", Toast.LENGTH_LONG).show();

            Uri uri = data.getData();
            StorageReference filepath = mStorage.child("photos").child(uri.getLastPathSegment());
            filepath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //String downloadUri = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                    String downloadUri = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                    Picasso.with(getActivity()).load(downloadUri).fit().centerCrop().into(pImageView);

                    Toast.makeText(getActivity(), "Uploading Finished...", Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}