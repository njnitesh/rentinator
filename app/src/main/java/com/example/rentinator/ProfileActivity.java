package com.example.rentinator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.example.rentinator.R;

public class ProfileActivity extends AppCompatActivity {

    EditText etnm,etmail,etcon;
    ImageView pImageView;
    Button btncamera,btngallery;
    DataSnapshot dataSnapshot;
    DatabaseReference rootRef, demoRef;
    public String usr,mail,contact;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
       // getSupportActionBar().setTitle("My Account");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etnm=(EditText)findViewById(R.id.editText);
        etmail=(EditText)findViewById(R.id.editText1);
        etcon=(EditText)findViewById(R.id.editText2);
        pImageView =(ImageView)findViewById(R.id.imageView);

        //database reference pointing to root of database
        rootRef = FirebaseDatabase.getInstance().getReference();

        //database reference pointing to demo node
        demoRef = rootRef.child("demo");

        demoRef.child("username").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                usr = dataSnapshot.getValue(String.class);
                etnm.setText(usr);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        demoRef.child("mail").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mail = dataSnapshot.getValue(String.class);
                etmail.setText(mail);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        demoRef.child("phno").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                contact = dataSnapshot.getValue(String.class);
                etcon.setText(contact);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });



    }

   }
