package com.example.rentinator;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public  class ViewPageAdapter extends FragmentPagerAdapter {

   private  final List<Fragment> fragmentlist = new ArrayList<>() ;
   private  final List<String> FragmentListTitles = new ArrayList<>();

    public ViewPageAdapter(FragmentManager fm) {
        super(fm);
    }
    @Override
    public Fragment getItem(int position){
        return fragmentlist.get(position);
    }

    @Override
    public int getCount(){
        return FragmentListTitles.size();
    }

    @Override
    public CharSequence getPageTitle(int position){
        return FragmentListTitles.get(position);
    }

    public void AddFragment(Fragment fragment,String Title){
        fragmentlist.add(fragment);
        FragmentListTitles.add(Title);
    }





}
