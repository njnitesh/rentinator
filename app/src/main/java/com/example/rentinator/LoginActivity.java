package com.example.rentinator;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Scanner;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

public class LoginActivity extends AppCompatActivity {

    private EditText uName, password;
    private Button lBtn;
    private Firebase root;
    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        uName = findViewById(R.id.etEmail);
        password = findViewById(R.id.etPassword);
        lBtn = findViewById(R.id.buttonlog);

        auth = FirebaseAuth.getInstance();

        String id = "";

        try {
            id = ReadData();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Toast.makeText(this, id, Toast.LENGTH_SHORT).show();
        final String finalId = id;
        lBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(uName.getText().toString()) || TextUtils.isEmpty(password.getText().toString())) {
                    Toast.makeText(LoginActivity.this, "Empty fields are not allowed", Toast.LENGTH_SHORT).show();
                }
                else {
                    SignIn(uName.getText().toString(),password.getText().toString(), root, finalId);
                }
            }
        });

    }

    private void SignIn(final String uName, final String password, Firebase root, String id){

        root = new Firebase("https://rentinator-5ff21.firebaseio.com/users/" + id);

        root.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    Map<String,String> map = (Map) dataSnapshot.getValue(Map.class);

                    String email = map.get("Email").toString();
                    String pwd = map.get("Password").toString();
                    String name = map.get("Name").toString();

                    if(email.equals(uName) && pwd.equals(password)){
                        Toast.makeText(LoginActivity.this, "Welcome "+name, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(),TabbedActivity.class);
                        finish();
                        startActivity(intent);

                    }else{
                        Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
                    }

                }
                catch(Exception e){
                    Toast.makeText(getApplicationContext(),e.getMessage()
                            ,Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
    private String ReadData() throws IOException {

        File file = new File(getFilesDir()+"/userId.txt");
        Scanner sc = new Scanner(file);

        sc.useDelimiter("\\Z");

        String data = sc.next();
        return data;
    }

}
