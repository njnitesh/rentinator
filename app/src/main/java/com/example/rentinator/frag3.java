package com.example.rentinator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.rentinator.R.layout.custom;

public class frag3 extends Fragment {
    View view;
    int[] IMAGES = {R.drawable.cars, R.drawable.bikes, R.drawable.mobile, R.drawable.furnitures, R.drawable.electronic, R.drawable.stationery, R.drawable.fashion,R.drawable.sp};
    String[] NAMES = {"Cars", "Bikes", "Mobiles", "Furniture", "Electroincs & Appliances", "Stationary", "Fashion","Sports"};
    Activity context;
     public static String TAG="Frag3";
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view=inflater.inflate(R.layout.frag3_layout, container, false);
        ListView listView = (ListView) view.findViewById(R.id.list_view);
        frag3.CustomAdapter customAdapter = new frag3.CustomAdapter();
        listView.setAdapter(customAdapter);
        context = getActivity();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String tempHolder = NAMES[position];
                Intent intent = new Intent(context,frag3test.class);
                intent.putExtra("val",tempHolder);
                startActivity(intent);

            }
        });
        return view;
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return IMAGES.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            view = LayoutInflater.from(getActivity()).inflate(R.layout.custom, null);
            //Toast.makeText(getActivity(),position,Toast.LENGTH_SHORT).show();

            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
            TextView textViewName = (TextView) view.findViewById(R.id.textViewName);

            imageView.setImageResource(IMAGES[position]);
            textViewName.setText(NAMES[position]);

            Log.d(TAG, "getView: "+position);

            return view;
        }

    }
}
