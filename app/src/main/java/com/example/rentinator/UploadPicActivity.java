package com.example.rentinator;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.example.rentinator.R;

public class UploadPicActivity extends AppCompatActivity {

    private Button mUploadBtn;
    private ImageView mImageView;

    private static final int CAMERA_REQUEST_CODE = 1;

    private StorageReference mStorage;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_pic);

        mStorage = FirebaseStorage.getInstance().getReference();


        mUploadBtn = findViewById(R.id.upload);
        mImageView = findViewById(R.id.imageView);
        mUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    startActivityForResult(intent, CAMERA_REQUEST_CODE);

                }



        });
    }

    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {

            Toast.makeText(UploadPicActivity.this, "Uploading Image...", Toast.LENGTH_LONG).show();

            Uri uri = data.getData();
            StorageReference filepath = mStorage.child("photos").child(uri.getLastPathSegment());
            filepath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //String downloadUri = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                    String downloadUri = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                    Picasso.with(UploadPicActivity.this).load(downloadUri).fit().centerCrop().into(mImageView);

                    Toast.makeText(UploadPicActivity.this, "Uploading Finished...", Toast.LENGTH_LONG).show();
                }
            });
        }


    }
}
