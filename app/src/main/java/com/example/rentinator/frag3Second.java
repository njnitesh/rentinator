package com.example.rentinator;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class frag3Second extends Fragment {
    TextView textView;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View view;
        super.onCreate(savedInstanceState);

        view=inflater.inflate(R.layout.frag3second, container, false);

        Bundle bundle = getActivity().getIntent().getExtras();

        String tempHolder = bundle.getString("val");
        Toast.makeText(this.getContext(), tempHolder, Toast.LENGTH_SHORT).show();
        return view;
    }
}
